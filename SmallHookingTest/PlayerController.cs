﻿using UnityEngine;
using Vector2 = UnityEngine.Vector2;

// Just an example of required componets to auto import the required components
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    private Vector2 inputHeading;
    private bool requestingJump = false;

    [SerializeField] // Serialize data but have it show up on the editor as well helps presever encapsulation
    private float jumpForce;

    [SerializeField] private float horizontalVelocityModifier;

    private Rigidbody2D playerRigidbody = null;

    private Transform transformComponent = null;

    // Simple state, we might want in the future to switch to a state machine type system if we have complex behaviours such as celeste
    private bool isGrounded = true;

    [SerializeField] private float distanceToRaycast;

    // Start is called before the first frame update
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        transformComponent = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessFrameInput();

        ProcessFrameMovement();

        ProcessIsGrounded();
    }

    void ProcessFrameInput()
    {
        inputHeading = GetHeadingFromInput();
        requestingJump = GetJumpStateFromInput();
    }

    Vector2 GetHeadingFromInput()
    {
        // This should be based off some sort of mapped Gestures system instead of direct keys
        return new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }

    bool GetJumpStateFromInput()
    {
        // This should be based off some sort of mapped Gestures system instead of direct keys
        return Input.GetKeyDown(KeyCode.Space);
    }

    void ProcessFrameMovement()
    {
        if (isGrounded)
        {
            Vector2 calculatedVelocity = new Vector2
            (
                // just apply the explected x velocity here nothing crazy 
                inputHeading.x * horizontalVelocityModifier,
                playerRigidbody.velocity.y
            );

            playerRigidbody.velocity = calculatedVelocity;

            if (requestingJump)
            {
                playerRigidbody.AddForce(Vector2.up * jumpForce);
            }
        }
        else
        {
            // Do nothing for now
        }
    }

    void ProcessIsGrounded()
    {
        Debug.DrawLine( transformComponent.position, new Vector2(transformComponent.position.x, transformComponent.position.y) + (Vector2.down * distanceToRaycast), Color.red );
        RaycastHit2D rayResult = Physics2D.Raycast(transformComponent.position, Vector2.down * distanceToRaycast);
        if (rayResult.collider != null)
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }
}
