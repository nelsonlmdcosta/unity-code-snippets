﻿using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;

public class GrappleComponent : MonoBehaviour
{
    enum HookState
    {
        Idle,
        Shooting,
        Hooked,
        Retracting
    }

    private HookState currentHookState;

    // Actor Component References
    private Rigidbody2D actorRigidbody;
    private Transform actorTransform;

    // GrapplePoint Component References
    [SerializeField] private Rigidbody2D grapplePointRigidbody;
    private Transform grapplePointTransform;
    private DistanceJoint2D distanceJointComponent;

    // Grapple Variables
    [SerializeField]
    private float pullingForce = 5;
    [SerializeField]
    private float minForceModifier = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        actorRigidbody = GetComponent<Rigidbody2D>();
        actorTransform = GetComponent<Transform>();

        InitializeGrapplePoint();

        InitializeHingeJoint();
    }

    void InitializeGrapplePoint()
    {
        // Check if we dont have an override, if not then create it
        if (!grapplePointRigidbody)
        {
            // Create said gameobject
            GameObject basicGameObject = new GameObject();
            grapplePointRigidbody = basicGameObject.AddComponent<Rigidbody2D>();
            distanceJointComponent = basicGameObject.AddComponent<DistanceJoint2D>();

            grapplePointRigidbody.isKinematic = true;
        }

        // if we still dont have it turn this off and throw an error message because something went horribly wrong
        if (!grapplePointRigidbody)
        {
            Debug.LogError(gameObject.name + " Has Been Encountered An Error Creating Or Finding A Hinge GameObject In The Scene");
            enabled = false;
            return;
        }

        // Get The Transform As Well
        grapplePointTransform = grapplePointRigidbody.gameObject.transform;
    }

    void InitializeHingeJoint()
    {
        if (distanceJointComponent)
        {
            // Also turn off the hingejoing
            distanceJointComponent.connectedBody = actorRigidbody;
            distanceJointComponent.maxDistanceOnly = true;
            distanceJointComponent.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInputToStates();

        ProcessStateMachine();
    }

    void ProcessInputToStates()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            SetNewState(HookState.Shooting);
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            SetNewState(HookState.Retracting);
        }
    }

    void ProcessStateMachine()
    {
        switch (currentHookState)
        {
            case HookState.Idle:
            {
                break;
            }
            case HookState.Shooting:
            {
                break;
            }
            case HookState.Hooked:
            {
                // This can get bonkers so it would need some reduction in force in the equation that or limit how far the grapple can go
                Vector2 actorToGrappleDirection = (grapplePointTransform.position - actorTransform.position);
                float distanceToGrapplePointSqr = actorToGrappleDirection.sqrMagnitude;

                float forceModifier = minForceModifier + ( pullingForce * distanceToGrapplePointSqr );

                actorRigidbody.AddForce(actorToGrappleDirection.normalized * forceModifier);

                break;
            }
            case HookState.Retracting:
            {
                break;
            }
        }
    }

    void SetNewState(HookState newState)
    {
        // No need to reset states
        if (currentHookState == newState)
        {
            return;
        }

        HookState oldState = currentHookState;
        currentHookState = newState;


        // Run Cleanup Of Current State If Any
        switch (oldState)
        {
            case HookState.Idle:
            {
                break;
            }
            case HookState.Shooting:
            {
                break;
            }
            case HookState.Hooked:
            {
                break;
            }
            case HookState.Retracting:
            {
                break;
            }
        }

        // Setup New State
        switch (currentHookState)
        {
            case HookState.Idle:
            {
                break;
            }
            case HookState.Shooting:
            {
                Vector2 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (grapplePointTransform)
                {
                    grapplePointTransform.position = position;

                    // No Special Animations Just Immediately Go On To Being Hooked
                    SetNewState(HookState.Hooked);
                }

                if (distanceJointComponent)
                {
                    distanceJointComponent.enabled = true;
                }

                    break;
            }
            case HookState.Hooked:
            {
                break;
            }
            case HookState.Retracting:
            {
                if (distanceJointComponent)
                {
                    distanceJointComponent.enabled = false;
                }

                // Direct Transition Because We Dont Care About Making The Object Lerp To A Point Yet
                SetNewState(HookState.Idle);

                break;
            }
        }
    }
}
