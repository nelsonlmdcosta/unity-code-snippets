﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
public class Generate : MonoBehaviour 
{
    [Header("Dimensions And Materials")]
    [Tooltip("Square Dimension From 0 to 255")]
    [SerializeField] [Range(0,254)]private int dimension;
    [SerializeField] private Material baseMaterial;

    [Header("Perlin Control Variables")]
    [Tooltip("Perlin Offset (To Not Cause Weird Symtries)")]
    [SerializeField] private Vector3 WorldOffset;
    [Tooltip("Modifies The Zoom Of The Perlin Equation")]
    [SerializeField] [Range(0, 1)] private float perlinZoom;
    [Tooltip("Modifies The Height Of The Generated Perlin Value")]
	[SerializeField] private float yHeightMultiplier;

	private List<Vector3> vertices = new List<Vector3>();
	private List<Vector3> normals = new List<Vector3>();
	private List<Color> vertexColors = new List<Color>();
	private List<int> triangles = new List<int>();

    private Transform myTransform = null;

    public bool shouldUseUpdate = false;

	private float[,] perlinNoiseMap;
	private float[,] gradientMap;

	[SerializeField] private float gradientZoom;
	[SerializeField] private Gradient gradientColor;
    [SerializeField] private AnimationCurve curve;

    [Header("LibNoise")]
    [SerializeField] private bool useLibNoise = false;
    [SerializeField] private double frequency = 1;
    [SerializeField] private double lacunarity = 2;
    [SerializeField] private double persistance = 0.5;
    [SerializeField] private int octaves = 6;
    [SerializeField] private string seed = "";
    [SerializeField] private LibNoise.QualityMode qualityMode = LibNoise.QualityMode.Medium;

    [SerializeField] private Vector3 direction;

    LibNoise.Generator.Perlin perlinNoise;

	private Texture2D noise;

    private void Start()
	{
        myTransform = GetComponent<Transform>();

        WorldOffset = -myTransform.position;

        perlinNoise = new LibNoise.Generator.Perlin(frequency, lacunarity, persistance, octaves, seed.GetHashCode(), qualityMode);

		GenerateCircularGradient();
		GeneratePerlinNoise();

        GenerateMesh();

        HeightMapToFile();
    }

    private void Update()
    {
        if (shouldUseUpdate)
        {
            WorldOffset += direction;

			GenerateCircularGradient();
			GeneratePerlinNoise();

            HeightMapToFile();


			GenerateMesh();
           
			GetComponent<MeshRenderer>().material.SetTexture("_main", noise);
        }

    }

    private void GenerateMesh()
    {
        // Generate All Verticies And Colors
        for (int z = 0; z < dimension; z++)
        {
            for (int x = 0; x < dimension; x++)
            {
                //float noise = perlinNoiseMap[x, z];
                float noise = gradientMap[x, z] - perlinNoiseMap[x, z];

                vertices.Add(new Vector3(x, noise * yHeightMultiplier, z));

                normals.Add(Vector3.up);
                vertexColors.Add( gradientColor.Evaluate(noise));
            }
        }

        for (int z = 0; z < dimension - 1; z++)
        {
            for (int x = 0; x < dimension - 1; x++)
            {
                triangles.Add((int)(x + dimension * z));
                triangles.Add((int)(x + dimension * z + dimension));
                triangles.Add((int)(x + dimension * z + dimension + 1));

                triangles.Add((int)(x + dimension * z));
                triangles.Add((int)(x + dimension * z + dimension + 1));
                triangles.Add((int)(x + dimension * z + 1));
            }
        }

        Mesh mesh = new Mesh();

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();

        mesh.colors = vertexColors.ToArray();
        mesh.normals = normals.ToArray();

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        GetComponent<MeshFilter>().mesh = mesh;
        GetComponent<MeshRenderer>().material = baseMaterial;
        GetComponent<MeshCollider>().sharedMesh = mesh;

        // Clear Up Memory
        vertices.Clear();
        normals.Clear();
        vertexColors.Clear();
        triangles.Clear();
    }

	private void GenerateCircularGradient()
	{
		gradientMap = new float[dimension,dimension];

		// Circular Gradient
		for (int z = 0; z < dimension; z++)
		{
			for (int x = 0; x < dimension; x++)
			{
				gradientMap[x, z] = 1 - (Vector2.Distance(new Vector2(x, z), new Vector2(dimension / 2, dimension / 2)) / dimension) / gradientZoom;
			}
		}
	}

	private void GeneratePerlinNoise()
	{
		perlinNoiseMap = new float[dimension,dimension];
		// Generate All Verticies And Colors
		for (int z = 0; z < dimension; z++)
		{
			for (int x = 0; x < dimension; x++)
			{
                if (!useLibNoise)
                    perlinNoiseMap[x, z] = Mathf.PerlinNoise((x - WorldOffset.x) / dimension / perlinZoom, (z - WorldOffset.z) / dimension / perlinZoom);
                else perlinNoiseMap[x, z] = (float)perlinNoise.GetValue(x - WorldOffset.x, 0, z - WorldOffset.z);

                //perlinNoiseMap[x, z] = (perlinNoiseMap[x, z] - curve.Evaluate(perlinNoiseMap[x, z])) / 2;
            }
        }
	}

    private void HeightMapToFile()
    {
        noise = new Texture2D(dimension, dimension, TextureFormat.RGB24, false);



        for (int z = 0; z < dimension; z++)
        {
            for (int x = 0; x < dimension; x++)
            {
                noise.SetPixel(x, z, gradientColor.Evaluate(Mathf.Clamp01(gradientMap[x, z] - perlinNoiseMap[x, z])));
            }
        }

        noise.Apply();


        // Encode texture into PNG
        byte[] bytes = noise.EncodeToPNG();

        // For testing purposes, also write to a file in the project folder
        File.WriteAllBytes(Application.dataPath + "/Noise.png", bytes);
    }
}

