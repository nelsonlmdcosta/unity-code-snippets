﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(MeshRenderer))]
public class UnityPerlin : MonoBehaviour
{
    [SerializeField] private Vector3 perlinOffset;
    [SerializeField] [Range(10,255)] private int dimensions;
    [SerializeField] [Range(0, 1)] private float perlinZoom;
    [SerializeField] [Range(1, 5)]private int yHeightMultiplier;

    [SerializeField] private Material baseMaterial;

    private List<Vector3> vertices = new List<Vector3>();
    private List<Vector3> normals = new List<Vector3>();
    private List<Color> vertexColors = new List<Color>();
    private List<int> triangles = new List<int>();
    private List<Vector2> uvs = new List<Vector2>();

    private int[,] heightMap;

	private void Start ()
    {
        //=========================//
        //  Calculate Height Map   //
        //=========================//
        heightMap = new int[dimensions, dimensions];

        for (int z = 0; z < dimensions; z++)
        {
            for (int x = 0; x < dimensions; x++)
            {
                heightMap[x, z] = Mathf.FloorToInt(Mathf.PerlinNoise((x - perlinOffset.x) / dimensions / perlinZoom, (z - perlinOffset.z) / dimensions / perlinZoom) * yHeightMultiplier * 10); // 10 aligns to unity units
            }
        }

        //=========================//
        //     Calculate Sides     //
        //=========================//
        CalculateExternalSides();

        //=========================//
        //      Calculate Top      //
        //=========================//
        CalculateLand( vertices.Count, triangles.Count, normals.Count, uvs.Count );

        Mesh mesh = new Mesh();

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.normals = normals.ToArray();
        mesh.uv = uvs.ToArray();

        GetComponent<MeshFilter>().mesh = mesh;
    }

    private void CalculateExternalSides()
    {
        // South Face
        for (int x = 0; x < dimensions; x++)
        {
            vertices.Add(new Vector3(x, 0, 0));
            vertices.Add(new Vector3(x, heightMap[x, 0], 0));
            vertices.Add(new Vector3(x + 1, heightMap[x, 0], 0));
            vertices.Add(new Vector3(x + 1, 0, 0));

            triangles.Add(vertices.Count - 4);
            triangles.Add(vertices.Count - 3);
            triangles.Add(vertices.Count - 2);

            triangles.Add(vertices.Count - 4);
            triangles.Add(vertices.Count - 2);
            triangles.Add(vertices.Count - 1);

            normals.Add(Vector3.back);
            normals.Add(Vector3.back);
            normals.Add(Vector3.back);
            normals.Add(Vector3.back);

            uvs.Add(new Vector2(0.0f, 0.0f));
            uvs.Add(new Vector2(0.0f, heightMap[x, 0]));
            uvs.Add(new Vector2(0.5f, heightMap[x, 0]));
            uvs.Add(new Vector2(0.5f, 0.0f));
        }

        // East Face
        for (int z = 0; z < dimensions; z++)
        {
            vertices.Add(new Vector3(dimensions, 0, z));
            vertices.Add(new Vector3(dimensions, heightMap[dimensions-1, z], z));
            vertices.Add(new Vector3(dimensions, heightMap[dimensions-1, z], z + 1));
            vertices.Add(new Vector3(dimensions, 0, z + 1));

            triangles.Add(vertices.Count - 4);
            triangles.Add(vertices.Count - 3);
            triangles.Add(vertices.Count - 2);

            triangles.Add(vertices.Count - 4);
            triangles.Add(vertices.Count - 2);
            triangles.Add(vertices.Count - 1);

            normals.Add(Vector3.right);
            normals.Add(Vector3.right);
            normals.Add(Vector3.right);
            normals.Add(Vector3.right);

            uvs.Add(new Vector2(0.0f, 0.0f));
            uvs.Add(new Vector2(0.0f, heightMap[dimensions-1, z]));
            uvs.Add(new Vector2(0.5f, heightMap[dimensions-1, z]));
            uvs.Add(new Vector2(0.5f, 0.0f));
        }

        // North Face
        for (int x = 0 ; x < dimensions; x++)
        {
            vertices.Add(new Vector3(x, 0, dimensions));
            vertices.Add(new Vector3(x, heightMap[x, dimensions - 1], dimensions));
            vertices.Add(new Vector3(x + 1, heightMap[x, dimensions - 1], dimensions));
            vertices.Add(new Vector3(x + 1, 0, dimensions));

            triangles.Add(vertices.Count - 2);
            triangles.Add(vertices.Count - 3);
            triangles.Add(vertices.Count - 1);

            triangles.Add(vertices.Count - 4);
            triangles.Add(vertices.Count - 1);
            triangles.Add(vertices.Count - 3);

            normals.Add(Vector3.forward);
            normals.Add(Vector3.forward);
            normals.Add(Vector3.forward);
            normals.Add(Vector3.forward);

            uvs.Add(new Vector2(0.0f, 0.0f));
            uvs.Add(new Vector2(0.0f, heightMap[x, dimensions-1]));
            uvs.Add(new Vector2(0.5f, heightMap[x, dimensions-1]));
            uvs.Add(new Vector2(0.5f, 0.0f));
        }

        // West Face
        for (int z = 0; z < dimensions; z++)
        {
            vertices.Add(new Vector3(0, 0, z));
            vertices.Add(new Vector3(0, heightMap[0, z], z));
            vertices.Add(new Vector3(0, heightMap[0, z], z+1));
            vertices.Add(new Vector3(0, 0, z+1));

            triangles.Add(vertices.Count - 1);
            triangles.Add(vertices.Count - 2);
            triangles.Add(vertices.Count - 3);

            triangles.Add(vertices.Count - 1);
            triangles.Add(vertices.Count - 3);
            triangles.Add(vertices.Count - 4);

            normals.Add(Vector3.left);
            normals.Add(Vector3.left);
            normals.Add(Vector3.left);
            normals.Add(Vector3.left);

            uvs.Add(new Vector2(0.0f, 0.0f));
            uvs.Add(new Vector2(0.0f, heightMap[0, z]));
            uvs.Add(new Vector2(0.5f, heightMap[0, z]));
            uvs.Add(new Vector2(0.5f, 0.0f));
        }
    }

    private void CalculateLand(int vertexOffset, int triangleOffset, int normalOffset, int uvsOffset)
    {
        // Generate Top Part
        for (int z = 0; z < dimensions; z++)
        {
            for (int x = 0; x < dimensions; x++)
            {
                vertices.Add(new Vector3(x, heightMap[x, z], z));
                vertices.Add(new Vector3(x, heightMap[x, z], z + 1));
                vertices.Add(new Vector3(x + 1, heightMap[x, z], z + 1));
                vertices.Add(new Vector3(x + 1, heightMap[x, z], z));

                triangles.Add(vertices.Count - 4);
                triangles.Add(vertices.Count - 3);
                triangles.Add(vertices.Count - 2);

                triangles.Add(vertices.Count - 4);
                triangles.Add(vertices.Count - 2);
                triangles.Add(vertices.Count - 1);

                normals.Add(Vector3.up);
                normals.Add(Vector3.up);
                normals.Add(Vector3.up);
                normals.Add(Vector3.up);

                uvs.Add(new Vector2(0.5f, 0));
                uvs.Add(new Vector2(0.5f, 1));
                uvs.Add(new Vector2(1.0f, 1));
                uvs.Add(new Vector2(1.0f, 0));
            }
        }

        // Generate Internal Sides
        for (int z = 0; z < dimensions; z++)
        {
            for (int x = 0; x < dimensions; x++)
            {
                if (z < dimensions - 1)
                {
                    if (heightMap[x, z] != heightMap[x, z + 1])
                    {
                        vertices.Add(new Vector3(x    , heightMap[x, z]    , z + 1));
                        vertices.Add(new Vector3(x    , heightMap[x, z + 1], z + 1));
                        vertices.Add(new Vector3(x + 1, heightMap[x, z + 1], z + 1));
                        vertices.Add(new Vector3(x + 1, heightMap[x, z]    , z + 1));

                        triangles.Add(vertices.Count - 4);
                        triangles.Add(vertices.Count - 3);
                        triangles.Add(vertices.Count - 2);

                        triangles.Add(vertices.Count - 4);
                        triangles.Add(vertices.Count - 2);
                        triangles.Add(vertices.Count - 1);

                        normals.Add(Vector3.up);
                        normals.Add(Vector3.up);
                        normals.Add(Vector3.up);
                        normals.Add(Vector3.up);

                        uvs.Add(new Vector2(0.5f, 0));
                        uvs.Add(new Vector2(0.5f, Mathf.Abs(heightMap[x, z] - heightMap[x, z + 1])));
                        uvs.Add(new Vector2(1.0f, Mathf.Abs(heightMap[x, z] - heightMap[x, z + 1])));
                        uvs.Add(new Vector2(1.0f, 0));
                    }
                }

                if (x < dimensions - 1)
                {
                    if (heightMap[x, z] != heightMap[x + 1, z])
                    {
                        vertices.Add(new Vector3(x + 1, heightMap[x, z], z));
                        vertices.Add(new Vector3(x + 1, heightMap[x + 1, z], z));
                        vertices.Add(new Vector3(x + 1, heightMap[x + 1, z], z + 1));
                        vertices.Add(new Vector3(x + 1, heightMap[x, z], z + 1));

                        triangles.Add(vertices.Count - 2);
                        triangles.Add(vertices.Count - 3);
                        triangles.Add(vertices.Count - 4);

                        triangles.Add(vertices.Count - 1);
                        triangles.Add(vertices.Count - 2);
                        triangles.Add(vertices.Count - 4);

                        normals.Add(Vector3.up);
                        normals.Add(Vector3.up);
                        normals.Add(Vector3.up);
                        normals.Add(Vector3.up);

                        uvs.Add(new Vector2(0.5f, 0));
                        uvs.Add(new Vector2(0.5f, Mathf.Abs(heightMap[x, z] - heightMap[x + 1, z])));
                        uvs.Add(new Vector2(1.0f, Mathf.Abs(heightMap[x, z] - heightMap[x + 1, z])));
                        uvs.Add(new Vector2(1.0f, 0));
                    }
                }
            }
        }
    }
}
